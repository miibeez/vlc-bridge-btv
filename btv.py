import requests
import json
import os
import time
import uuid
from threading import Lock

class Client:
    def __init__(self):
        self.auth = None
        self.token = ""
        self.activation = None
        self.mutex = Lock()

    def channels(self):
        with self.mutex:
            err = self.login()
            if err:
                return None, err

            resp = requests.get(
                "https://api.mybtv.net/v2/lineups",
                headers={
                    "Authorization": f"Bearer {self.token}",
                    "User-Agent": "Dalvik/2.1.0 (Linux; U; Android 5.1.1; AFTT Build/LVY48F)"
                }
            )
            if resp.status_code != 200:
                return None, resp.text
            resp = resp.json()

            stations = []
            for lineup in resp["lineups"]:
                for s in lineup["stations"]:
                    stations.append({
                        "id": s["name"],
                        "name": f"{s['channel']} {s['callsign']}",
                        "logo": s["logo_url"],
                        "watchId": f"btv-{s['id']}",
                    })
            return stations, None

    def watch(self, id):
        with self.mutex:
            err = self.login()
            if err:
                return None, err

            resp = requests.get(
                "https://api.mybtv.net/v3/airings",
                params={
                    "channels": id[4:],
                    "start": time.strftime("%Y-%m-%dT%H:%M:%S%z", time.gmtime()),
                    "duration": "60"
                },
                headers={
                    "Authorization": f"Bearer {self.token}",
                    "User-Agent": "Dalvik/2.1.0 (Linux; U; Android 5.1.1; AFTT Build/LVY48F)"
                }
            )
            if resp.status_code != 200:
                return None, resp.text
            resp = resp.json()

            for airing in resp["airings"]:
                return airing["playback"]["url"], None
            return None, "no airing found"

    def activation_code(self):
        id = str(uuid.uuid4())
        resp = requests.post(
            "https://api.mybtv.net/v2/devices/create_code",
            json={"id": id, "name": "AFTT"},
            headers={
                "User-Agent": "Dalvik/2.1.0 (Linux; U; Android 5.1.1; AFTT Build/LVY48F)"
            }
        )
        if resp.status_code != 200:
            return "", "", resp.text
        resp = resp.json()
        return id, resp["activation_code"], None

    def check_activation_status(self, id, code):
        resp = requests.get(
            "https://api.mybtv.net/v2/devices/check_code",
            params={"code": code, "id": id},
            headers={
                "User-Agent": "Dalvik/2.1.0 (Linux; U; Android 5.1.1; AFTT Build/LVY48F)"
            }
        )
        if resp.status_code != 200:
            return None, resp.text
        resp = resp.json()
        return resp, None

    def login(self):
        if self.auth and self.auth["access_token"] is not None:
            self.token = self.auth["access_token"]
            return None

        try:
            with open("btv-auth.json", "r") as f:
                self.auth = json.load(f)
        except FileNotFoundError:
            print("Activation needed")
            self.auth = None

        if not self.auth:
            return "activation needed"
        self.token = self.auth["access_token"]
        return None

    def activated(self):
        if self.auth:
            return True, None
        return False, self.activation

    def activate(self):
        with self.mutex:
            resp, err = self.activate_loop()
            if err:
                return err

            with open("btv-auth.json", "w") as f:
                json.dump(resp, f)
            self.auth = resp
            self.token = self.auth["access_token"]
        return None

    def activate_loop(self):
        self.activation = "Loading..."
        id, code, err = self.activation_code()
        if err:
            print(f"Activation failed to start, err: {err}")
            return None, err

        url = "https://app.mybtv.net/activate"
        print(f"Activation code: {code}, url: {url}")
        self.activation = f"Visit {url} and enter code {code}"

        for i in range(20):
            resp, err = self.check_activation_status(id, code)
            if err:
                print(f"Activation check failed, err: {err}")
                return None, err

            if resp["status"] == "activated":
                print("Activation done!")
                self.activation = ""
                return resp, None

            time.sleep(6)

        self.activation = ""
        return None, "activation failed"
