# vlc-bridge-btv

watch LocalBTV live stream in VLC

### using

`$ docker run -d -p 7777:7777 --name vlc-bridge-btv registry.gitlab.com/miibeez/vlc-bridge-btv`

setup on http://localhost:7777

`$ vlc http://localhost:7777/btv/playlist.m3u`

### require

localbtv account + service area
